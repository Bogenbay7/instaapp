//
//  ViewController.swift
//  InstaApp
//
//  Created by Nurba on 24.02.2021.
//
import FirebaseAuth
import UIKit

class ViewController: UIViewController {

    
    private var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(IgFeedPostTableViewCell.self, forCellReuseIdentifier: IgFeedPostTableViewCell.identifier)
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
       
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        handleNotAuth()
        
       
    }


    private func handleNotAuth() {
        if Auth.auth().currentUser == nil{
            let loginVC = LoginViewController()
            loginVC.modalPresentationStyle = .fullScreen
            present(loginVC, animated: false)
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: IgFeedPostTableViewCell.identifier, for: indexPath) as! IgFeedPostTableViewCell
        
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 0
    }
    
    
}
