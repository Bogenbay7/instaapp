//
//  EditProfileViewController.swift
//  InstaApp
//
//  Created by Nurba on 24.02.2021.
//

import UIKit

struct EditProfileFormMode {
    let label:String
    let placeholder: String
    var value:String?
}


final class EditProfileViewController: UIViewController, UITableViewDataSource {
    
    let tableView:UITableView = {
        let tableView = UITableView()
        tableView.register(FormTableViewCell.self, forCellReuseIdentifier: FormTableViewCell.identifier)
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureModels()
        tableView.tableHeaderView = createTableHeaderView()
        tableView.dataSource = self
        view.addSubview(tableView)
        view.backgroundColor = .systemBackground
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(didTapSave))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(didTapCancel))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
    private var models = [[EditProfileFormMode]]()
    
    private func configureModels(){
        let section1Label = ["Name", "Username", "Bio"]
        var section1 = [EditProfileFormMode]()
        for label in section1Label{
            let model = EditProfileFormMode(label: label, placeholder: "Enter \(label)...", value: nil)
            section1.append(model)
        }
        models.append(section1)
        let section2Label = ["Email", "Phone", "Gender"]
        var section2 = [EditProfileFormMode]()
        for label in section2Label{
            let model = EditProfileFormMode(label: label, placeholder: "Enter \(label)...", value: nil)
            section2.append(model)
         
        }
        models.append(section2)
    }
    
    
    private func createTableHeaderView()->UIView{
        let header = UIView(frame: CGRect(x: 0, y: 0, width: view.width, height: view.height/4).integral)
            let size = header.height/1.5
        let profilePhotoButtom = UIButton(frame: CGRect(x: (view.width - size)/2, y: (header.height - size)/2, width: size, height: size))
        
        header.addSubview(profilePhotoButtom)
        profilePhotoButtom.layer.masksToBounds = true
        profilePhotoButtom.layer.cornerRadius = size/2.0
        profilePhotoButtom.addTarget(self, action: #selector(didTapProfilePhotoButton), for: .touchUpInside)
        profilePhotoButtom.setBackgroundImage(UIImage(systemName: "person.circle"), for: .normal)
        profilePhotoButtom.layer.borderWidth = 1
        profilePhotoButtom.layer.borderColor = UIColor.secondarySystemBackground.cgColor
        
        return header
    }
    
    @objc private func didTapProfilePhotoButton(){
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return models.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = models[indexPath.section][indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: FormTableViewCell.identifier, for: indexPath)as! FormTableViewCell
        
        cell.configure(with: model)
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard section == 1 else{
            return nil
        }
        return "Private information"
    
    }
    
    
    @objc private func didTapSave(){
        
    }
    
    @objc private func didTapCancel(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func didTapChangeProfilePhoto(){
        let actionSheet = UIAlertController(title: "Profile Picture", message: "Change Profile Picture", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {_ in
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Take from Library", style: .default, handler: {_ in
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        actionSheet.popoverPresentationController?.sourceView = view
        actionSheet.popoverPresentationController?.sourceRect = view.bounds
        
        present(actionSheet, animated: true)
    }

}
extension EditProfileViewController: FormTableViewCellDelegate{
    func formTableViewCell(_ cell: FormTableViewCell, didUpdateField updatedModel: EditProfileFormMode?) {
        print(updatedModel?.value ?? "nil")
    }
    
    
}
