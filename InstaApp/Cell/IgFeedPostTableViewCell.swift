//
//  IgFeedPostTableViewCell.swift
//  InstaApp
//
//  Created by Nurba on 25.02.2021.
//

import UIKit

final class IgFeedPostTableViewCell: UITableViewCell {
    
    static let identifier = "IgFeedPostTableViewCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
